# Kotlin cheatsheet

<!-- toc -->

- [Style](#style)
- [Basic types](#basic-types)
  * [Numbers](#numbers)
  * [Numeric constants](#numeric-constants)
  * [Type casting](#type-casting)
  * [Infix operations](#infix-operations)
  * [Characters](#characters)
  * [Booleans](#booleans)
  * [Arrays](#arrays)
    + [Immutable array](#immutable-array)
    + [Typed arrays](#typed-arrays)
  * [Strings](#strings)
    + [Variable interpolation](#variable-interpolation)
    + [Expression interpolation](#expression-interpolation)
- [Packages](#packages)
- [Imports](#imports)
- [Control flow](#control-flow)
  * [If](#if)
  * [When](#when)
  * [For](#for)
  * [While](#while)
  * [Return](#return)
    + [Lambda with explicit label](#lambda-with-explicit-label)
    + [Lambda with implicit label](#lambda-with-implicit-label)
    + [Anonymous function](#anonymous-function)
- [Classes and objects](#classes-and-objects)
  * [Primary constructor](#primary-constructor)
  * [Inheritance](#inheritance)
  * [Delegate to constructor](#delegate-to-constructor)
  * [Overriding](#overriding)
  * [Calling outer class method](#calling-outer-class-method)
  * [Abstract class](#abstract-class)
  * [Companion objects](#companion-objects)
  * [Properties and fields](#properties-and-fields)
- [Compile-time constants](#compile-time-constants)
- [Late-initialized properties and variables](#late-initialized-properties-and-variables)
- [Interfaces](#interfaces)
- [Visibility modifiers](#visibility-modifiers)
- [Extension functions](#extension-functions)
- [Extension properties](#extension-properties)
- [Data classes](#data-classes)
  * [Standard data classes](#standard-data-classes)
- [Sealed classes](#sealed-classes)
- [Generics](#generics)
- [Star projection](#star-projection)
- [Enum classes](#enum-classes)
- [Objects](#objects)
  * [Delegated properties](#delegated-properties)
    + [Lazy](#lazy)
    + [Observable](#observable)
- [Functions and lambdas](#functions-and-lambdas)
  * [Single-expresion functions](#single-expresion-functions)
  * [Variable number of arguments](#variable-number-of-arguments)
  * [Spread operator (*)](#spread-operator-)
  * [Infix notation](#infix-notation)
  * [Tail-recursive functions](#tail-recursive-functions)
  * [Higher-order functions](#higher-order-functions)
  * [Instantiating a function type](#instantiating-a-function-type)
  * [Passing a lambda to the last parameter](#passing-a-lambda-to-the-last-parameter)
  * [Inline functions](#inline-functions)
- [Destructuring declarations](#destructuring-declarations)
- [Collections](#collections)
- [Ranges](#ranges)
- [Type checking](#type-checking)
  * [Inference](#inference)
  * [`as` unsafe cast operator](#as-unsafe-cast-operator)
  * [`as?` safe (nullable) cast operator](#as-safe-nullable-cast-operator)
- [Qualified `this`](#qualified-this)
- [Equality](#equality)
  * [Structural equality (`equals()`)](#structural-equality-equals)
  * [Referential equality (`===`)](#referential-equality-)
- [Null safety](#null-safety)
  * [Safe calls (`?.`)](#safe-calls-)
  * [Elvis operator (`?:`)](#elvis-operator-)
  * [The `!!` operator](#the--operator)
- [Exceptions](#exceptions)
  * [`throw`](#throw)
- [Type aliases](#type-aliases)
- [Coroutines](#coroutines)
  * [`runBlocking`](#runblocking)
  * [`delay`](#delay)
  * [`coroutineScope`](#coroutinescope)
  * [Suspending functions](#suspending-functions)
  * [`launch`](#launch)
  * [Dispatchers](#dispatchers)
  * [`async`](#async)
  * [Other functions](#other-functions)
- [Standard library](#standard-library)
  * [Global](#global)
    + [Generic extension functions](#generic-extension-functions)
    + [Extension functions](#extension-functions-1)
    + [Functions](#functions)
  * [kotlin.collections](#kotlincollections)
    + [Classes](#classes)
    + [Properties](#properties)
    + [Functions that initialize collections](#functions-that-initialize-collections)
    + [Collection or array extension functions](#collection-or-array-extension-functions)
    + [Map extension functions](#map-extension-functions)
  * [kotlin.io](#kotlinio)
  * [kotlin.math](#kotlinmath)
    + [Properties](#properties-1)
    + [Extension properties](#extension-properties-1)
    + [Functions](#functions-1)
    + [Trigonometric functions](#trigonometric-functions)

<!-- tocstop -->

## Style

* Java-like
* Optional semicolons
* Optional parentheses for lambda expressions
* `=` shorthand syntax for single-line functions and getters
* Convention to use 4 spaces for indentation

## Basic types

### Numbers

| Type   | Bit width |
| ------ | --------- |
| Double | 64        |
| Float  | 32        |
| Long   | 64        |
| Int    | 32        |
| Short  | 16        |
| Byte   | 8         |

### Numeric constants

```kotlin
123
0x0F
123L
13.5f
13.5e10
val oneMillion = 1_000_000
```

### Type casting

* `toByte(): Byte`
* ...
* `toChar(): Char`

### Infix operations

* `shl`
* `shr`
* `ushl`
* `ushr`
* `and`
* `or`
* `xor`
* `inv`

### Characters

* `'1'`
* `\t \b \n \r \' \" \\ \$`
* `\uFF00`

```kotlin
'0'.toInt()
```

### Booleans

* `true`
* `false`
* `|| && !`

### Arrays

#### Immutable array

```kotlin
arrayOf(1, 2, 3)
```

#### Typed arrays

* ByteArray
* ...

```kotlin
val x: IntArray = intArrayOf(1, 2, 3)
```

### Strings

```kotlin
"abc"

val text = """
    for (c in "foo")
        print(c)
"""

val text = """
    | Tell me and I forget
""".trimMargin()
```

#### Variable interpolation

```kotlin
val i = 10
println("i = $i")
```

#### Expression interpolation

```kotlin
val s = "abc"
println("${s.length}")
```

## Packages

* = .NET namespaces
* Placed on top of file; applies to whole file

```kotlin
package foo.bar
```

## Imports

```kotlin
import foo.bar
import foo.*
import bar.Bar as bBar
```

## Control flow

### If

```kotlin
val max = if (a > b) a else b
```

### When

```kotlin
when (x) {
  1 -> print("x == 1")
  2 -> print("x == 2")
  else -> {
    print("x is neither 1 nor 2")
  }
}
```

Expression is not required.

### For

* Can loop over ranges
* = .NET foreach

```kotlin
for (item in collection) print(item)
for (i in 1..3) {}
for (i in 6 downTo 0 step 2) {}
for (i in array.indices) {}
for ((index, value) in array.withIndex()) {}
```

### While

```kotlin
while (x > 0) {
  x --
}

do {
  val y = retrieveData()
} while (y != null) // y is visible here!
```

### Return

`return`
  * Returns from function
  * Does **not** consider _lambda expressions_
  * can use labels to return from lambda expressions

#### Lambda with explicit label

```kotlin
fun foo() {
  listOf(1, 2, 3, 4, 5).forEach lit@{
    if (it == 3) return@lit
    print(it)
  }
}
```

#### Lambda with implicit label

```kotlin
fun foo() {
  listOf(1, 2, 3, 4, 5).forEach {
    if (it == 3) return@forEach
  }
}
```

#### Anonymous function

```kotlin
fun foo() {
  listOf(1, 2, 3, 4, 5).forEach(fun(value: Int) {
    if (value == 3) return
    print(value)
  })
}
```

## Classes and objects

```kotlin
class Invoice { /* ... */ }
class Empty // Classes can be empty
```

### Primary constructor

```kotlin
class Person(firstName: String) { /* ... */ }
```

### Inheritance

```kotlin
open class Base // overridable
class Derived(p: Int): Base
```

### Delegate to constructor

```kotlin
constructor(ctx: Context): super(ctx)
```

### Overriding

```kotlin
override fun v() { /* ... */ }
```

### Calling outer class method

```kotlin
super@Bar.f()
```

### Abstract class

```kotlin
abstract class Foo {
  abstract fun f()
}
```

### Companion objects

* Holds static members
* A singleton instance

### Properties and fields

```kotlin
class Address {
  // var - mutable
  var name: String = // ...
  // val - immutable
  var simple: Int?
  val inferredType = 1

  // Getter
  var isEmpty: Boolean
    get() = this.size == 0

  var stringRepresentation: String
    get() = this.toString()
    set(value) {
      setDataFromString(value)
    }

  var setterVisibility: String = "abc"
    private set
}
```

## Compile-time constants

primitive type or string

```kotlin
const val SUBSYSTEM_DEPRECATED = "..."
```

## Late-initialized properties and variables

* unit tests
* dependency injections

```kotlin
public class MyTest {
  lateinit var subject: TestSubject
  // ...
}
```

## Interfaces

```kotlin
interface MyInterface {
  fun bar()
  fun foo() {
    // ...
  }
}
```

## Visibility modifiers

* private
* protected
* internal - visible to same module
* public (default)

## Extension functions

_Receiver_: the value of `this` in an extension function.

```kotlin
fun MutableList<Int>.swap(index1: Int, index2: Int) {
  val tmp = this[index1]
  this[index1] = this[index2]
}
```

Nullable receiver

```kotlin
fun Any?.toString(): String {
  if (this == null) return "null"
  return toString()
}
```

## Extension properties

```kotlin
val <T> List<T>.lastInde: Int
  get() = size - 1
```

## Data classes

```kotlin
data class Customer(val name: String, val email: String)
```

### Standard data classes

* `Pair`
* `Triple`

## Sealed classes

* Cannot have subclasses outside of file

## Generics

```kotlin
class Box<T>(t: T) {
  var value = t
}

val box: Box<Int> = Box<Int>(1)
val box = Box(1) // inferred type
```

## Star projection

* `Foo<*>`
  * `Foo<out T: TUpper>` → `Foo<out TUpper>`
  * `Foo<in T>` → `Foo<in Nothing>`
* `Function<*, String>` → `Function<in Nothing, String>`
* `Function<Int, *>` → `Function<Int, out Any?>`
* `Function<*, *>` → `Function<in Nothing, out Any?>`

## Enum classes

* More like classes than enums
* Cannot be instantiated, can only use defined members

```kotlin
enum class Direction {
  NORTH, SOUTH, WEST, EAST
}
```

Initialization

```kotlin
enum class Color(val rgb: Int) {
  RED(0xFF0000),
  GREEN(0x00FF00),
  BLUE(0x0000FF)
}
```

Anonymous classes

```kotlin
enum class ProtocolState {
  WAITING {
    override fun signal() = TALKING
  },
  TALKING {
    override fun signal() = WAITING
  };

  // Required semicolon to separate enum constant members from member definitions

  abstract fun signal(): ProtocolState
}
```

## Objects

Object expressions

* Anonymous class
* Single-use object

```kotlin
window.addMouseListener(object: MouseAdapter() {
  override fun mouseClicked(e: MouseEvent) {
    // ...
  }
})
```

Object declarations

* = singleton

```kotlin
object DataProviderManager {
  fun registerDataProvider(provider: DataProvider) {
    // ...
  }
}
```

Companion objects

* static methods / properties
* is a real object!

```kotlin
class MyClass {
  // name can be omitted
  companion object Factory {
    fun create(): MyClass = MyClass()
  }
}

val instance = MyClass.create()
```

### Delegated properties

* Defers to another object for a property's value.

#### Lazy

* Implements a lazy property

```kotlin
val lazyValue: String by lazy {
  "Hello"
}
```

#### Observable

* `Delegates.observable`
* `Delegates.vetoable`

```kotlin
import kotlin.properties.Delegates

class User {
    var name: String by Delegates.observable("<no name>") {
        prop, old, new ->
        println("$old -> $new")
    }
}

fun main(args: Array<String>) {
    val user = User()
    user.name = "first"
    user.name = "second"
}
```

## Functions and lambdas

`Unit`
* Return type of function without useful value
* Has single value `Unit`

### Single-expresion functions

```kotlin
fun double(x: Int): Int = x * 2
```

### Variable number of arguments

```kotlin
fun <T> asList(vararg ts: T): List<T> {
  val result = ArrayList<T>()
  for (t in ts) // ts is an Array
    result.add(t)
  return result
}
```

### Spread operator (*)

```kotlin
val a = arrayOf(1, 2, 3)
val list = asList(-1, 0, *a, 4)
```

### Infix notation

```kotlin
infix fun Int.shl(x: Int): Int { /* ... */ }
1 shl 2 // = 1.shl(2)
```

### Tail-recursive functions

```kotlin
val eps = 1e-10
tailrec fun findFixPoint(x: Double = 1.0): Double =
    if (Math.abs(x - Math.cos(x)) < eps) x
    else findFixPoint(Math.cos(x))
```

### Higher-order functions

```kotlin
fun <T, R> Collection<T>.fold(
  initial: R,
  combine: (acc: R, nextElement: T) -> R
): R {
  // ...
}
```

### Instantiating a function type

Lambda expression

```kotlin
{ a, b -> a + b }
```

Anonymous function

```kotlin
fun(s: string): Int { /* ... */ }
```

Static member functions

```kotlin
::isOdd
String::toInt
```

### Passing a lambda to the last parameter

* Can be outside parentheses
* If only argument, parentheses optional

```kotlin
val product = items.fold(1) { acc, e -> acc * e }

run { println("...") }
```

`it`
* Implicit name of single parameter

```kotlin
ints.filter { it > 0 }
```

### Inline functions

* Inline a function at the call site
* Same thread

```kotlin
inline fun <T> lock(lock: Lock, body: () -> T): T {
  // ...
}
```

```kotlin
lock(l) { foo() }

// Produces the code:
l.lock()
try {
  foo()
}
finally {
  l.unlock()
}
```

## Destructuring declarations

```kotlin
val (name, age) = person
```

* **Must** declare component1(), component2(), ...


## Collections

Immutable collections

* `List<out T>` - `listOf()`
* `Set<out T>` - `setOf()`
* `Map<K, out V>` - `mapOf(a to b, c to d)`

Mutable collections

* `MutableList<T>` - `mutableListOf()`
* `MutableSet<T>` - `mutableSetOf()`
* `MutableMap<K, V>` - `hashMapOf(a to b)`

## Ranges

Inclusive range

```kotlin
1..10

for (i in 1..10) {
  println(1)
}
```

Reverse range
```kotlin
for (i in 4 downTo 1) print(i)
```

Step

```kotlin
for (i in 1..4 step 2) print(i)

for (i in 4 downTo 1 step 2) print(i)
```

Exclusive end element

```kotlin
for (i in 1 until 10) println(i)
```

`in`/`!in` operators

```kotlin
if (x in 1..10) // ...
```

## Type checking

### Inference

```kotlin
if (obj !is String) {
  print("Not a String")
}
else {
  print(obj.length)
}
```

### `as` unsafe cast operator

Throws an error if casting is not possible.

```kotlin
val x: String = y as String
```

### `as?` safe (nullable) cast operator

```kotlin
val x: String? = y as? String
```

## Qualified `this`

```kotlin
class A { // implicit label @A
    inner class B { // implicit label @B
        fun Int.foo() { // implicit label @foo
            val a = this@A // A's this
            val b = this@B // B's this

            val c = this // foo()'s receiver, an Int
            val c1 = this@foo // foo()'s receiver, an Int

            val funLit = lambda@ fun String.() {
                val d = this // funLit's receiver
            }


            val funLit2 = { s: String ->
                // foo()'s receiver, since enclosing lambda expression
                // doesn't have any receiver
                val d1 = this
            }
        }
    }
}
```

## Equality

### Structural equality (`equals()`)

* `==`
* `!=`

### Referential equality (`===`)

* `===`
* `!==`

## Null safety

### Safe calls (`?.`)

```kotlin
val a = "Kotlin"
val b: String? = null
println(b?.length)
println(a?.length)
```

### Elvis operator (`?:`)

```kotlin
r ?: x
```

* If `r` is not null, use it, otherwise, use `x`.
* `throw` and `return` can be used.

```kotlin
val l: Int = if (b != null) b.length else -1
// equivalent to
val l = b?.length ?: -1
```

### The `!!` operator

Intentionally throws a `NullPointerException` when the value is null.

```kotlin
val l = b!!.length
```

## Exceptions

### `throw`

* An expression in Kotlin
* Has type `Nothing`

## Type aliases

```kotlin
typealias NodeSet = Set<Network.Node>
```

## Coroutines

* = generators / async-await / multi-threading support
* _Suspends_ functions

### `runBlocking`

* Run code in a coroutine scope
* Blocks the current thread

### `delay`

Makes the current thread sleep for x milliseconds

```kotlin
fun main(args: Array<String>) = runBlocking { // this: CoroutineScope
  // = this@CoroutineScope.launch
  launch { // launch new coroutine in the scope of runBlocking
    delay(1000L)
    println("World!")
  }
  println("Hello,")
}
```

### `coroutineScope`

Creates a new `CoroutineScope`.

### Suspending functions

* Functions that run inside coroutines
* Cannot be called outside a coroutine scope
* Can be called by other suspending functions

```kotlin
fun main(args: Array<String>) = runBlocking {
    launch { doWorld() }
    println("Hello,")
}

suspend fun doWorld() {
    delay(1000L)
    println("World!")
}
```

### `launch`

* Launch a coroutine
* Returns a `Job`
* Can accept a `Dispatcher` to specify which dispatcher should be called to launch the coroutine (possibly in a new thread).

```kotlin
launch (Dispatchers.Default) {
  // ...
}
```

### Dispatchers

* Dispatches coroutines

Default dispatchers:

* `Dispatchers.Default` - default global dispatcher
* `Dispatchers.Unconfined` - Do not use.
* `Dispatchers.IO` - use for IO tasks.
* `Dispatches.Main` - Context-specific. E.g. for Android, the UI thread. Use to run code on the main thread.

### `async`

* Similar to `launch`.
* Returns a `Deferred` (a promise to provide a result later)
  * A subclass of `Job`, but returns a result.

```kotlin
fun main(args: Array<String>) = runBlocking<Unit> {
    val time = measureTimeMillis {
        val one = async { doSomethingUsefulOne() }
        val two = async { doSomethingUsefulTwo() }
        println("The answer is ${one.await() + two.await()}")
    }
    println("Completed in $time ms")
}
```

* Can be specified with `CoroutineStart.LAZY` to execute lazily.


### Other functions

* `awaitAll`: Awaits for completion of given `Deferred` values
* `joinAll`
* `promise` (JS)
* `withTimeout`
* `withTimeoutOrNull`

## Standard library

### Global

#### Generic extension functions

* `apply`: calls function with value as `this`; returns value
* `run`: calls function with value as `this`; returns block result
* `also`: calls function with value as `it`; returns value
* `let`: calls function with value as `it`; returns block result

|                | `this`  | `it`   |
| -------------- | ------- | ------ |
| returns value  | `apply` | `also` |
| returns result | `run`   | `let`  |

#### Extension functions

* `isFinite`
* `isInfinite`
* `isNaN`
* `toBigDecimal`
* `toBigInteger`
* `toString`

#### Functions

* `arrayOf`
* `assert`
* `check`
* `enumValueOf`
* `error`
* `lazy`
* `repeat`
* `require`
* `run`
* `takeIf`
* `takeUnless`
* `TODO`
* `with`

### kotlin.collections

#### Classes

* `ArrayList`: a `MutableList` which uses a resizable array as its backing storage.
* `HashMap`: a `MutableMap`
* `HashSet`: a `MutableSet`
* `LinkedHashMap`: a `MutableMap`, additionally preserves insertion order
* `LinkedHashSet`

#### Properties

* `indices`: Returns the range of valid indices for the array.

#### Functions that initialize collections

* `arrayOf`
* `emptyList`
* `emptyMap`
* `hashMapOf`
* `hashSetOf`
* `linkedMapOf`
* `linkedSetOf`
* `linkedStringMapOf`
* `linkedStringSetOf`
* `listOf`
* `mapOf`
* `mutableListOf`
* `mutableMapOf`
* `setOf`
* `stringMapOf`
* `stringSetOf`

#### Collection or array extension functions

* `addAll`
* `all`
* `any`
* `asIterable`
* `asList`
* `asReversed`
* `average`
* `chunked`
* `component1`..`5` (destructuring)
* `contains` (`in` operator)
* `contentEquals`
* `count`
* `distinct`
* `elementAt`
* `filter`
* `find`
* `first`
* `firstOrNull`
* `flatMap`
* `flatten`
* `fold`
* `forEach`
* `indexOf`
* `intersect`
* `isEmpty`
* `isNotEmpty`
* `joinToString`
* `last`
* `map`
* `max`
* `min`
* `minus`: Set difference
* `none`
* `partition`
* `plus`: Set union
* `random`
* `reduce`
* `reverse`
* `reversed`
* `shuffle`
* `shuffled`
* `single`
* `singleOrNull`
* `slice`
* `sort`
* `sorted`
* `subtract`
* `sum`
* `take`
* `takeWhile`
* `to`X`Array`
* `toList`
* `toMutableList`
* `toSet`
* `union`
* `unzip`
* `windowed`
* `zip`

#### Map extension functions

* `containsKey`
* `get`
* `getOrNull`
* `getOrPut`
* `putAll`
* `remove`
* `set`
* `toMutableMap`

### kotlin.io

* `createTempDir`
* `createTempFile`
* `print`
* `println`
* `readLine`
* `use`

### kotlin.math

#### Properties

* `absoluteValue`
* `E`
* `PI`

#### Extension properties

* `sign`
* `ulp`
* `pow`

#### Functions

* `abs` / `sign`
* `ceil` / `floor` / `truncate`
* `exp` / `expm1` / `sqrt`
* `hypot`
* `IEEErem`
* `ln` / `ln1p` / `log` / `log10` / `log2`
* `min` / `max`

#### Trigonometric functions

* `sin`/`cos`/`tan`
* `asin`/`acos`/`atan`
* `atan2`
* `atanh`
